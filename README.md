RicochetRobots
==============

A simple implementation of Alex Randolph's board game Ricochet Robots including solver.

Compile
-------

Compile the Java code with the command:

```bash
javac -cp lib/commons-cli-1.3.1.jar -d bin src/info/riemannhypothesis/ricochetrobots/*.java
```

Run
---

To run, type

```bash
./solve.sh -b <board> -g
```

This will place four robots in random positions on the board, pick one of the targets and one of the robots to reach it, try to find a solution (within the move and time contraints specified), and then give a graphical representation of the moves.

E.g., if you want to solve the board 3 in the repository, simply run

```bash
./solve.sh -b boards/3.txt -g
```

For a full description of all available command line options, simply type
 
```bash
./solve.sh
```
