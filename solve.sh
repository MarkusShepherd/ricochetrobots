#!/usr/bin/env bash

java -ea -cp bin:lib/commons-cli-1.3.1.jar info.riemannhypothesis.ricochetrobots.Solver $@
